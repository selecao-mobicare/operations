package br.com.mobicare.operations.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.mobicare.operations.service.OperationsService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController("operations")
public class OperationController {

	@Autowired
	OperationsService service;

	@GetMapping(params = { "v1", "v2", "v3", "v4" })
	public ResponseEntity<?> get(@RequestParam(name = "v1") int v1, @RequestParam(name = "v2") int v2,
			@RequestParam(name = "v3") int v3, @RequestParam(name = "v4") int v4) {
		
		log.info("teste");
		return ResponseEntity.ok(service.process(v1, v2, v3, v4));
	}
}
