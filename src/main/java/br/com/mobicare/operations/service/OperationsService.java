package br.com.mobicare.operations.service;

import org.springframework.stereotype.Service;

@Service
public class OperationsService {

	public int process(int v1, int v2, int v3, int v4) {
		X x;
		int op = (v1 + v2 + v3 + v4) % 4;
		switch (op) {
		case 0: {
			x = new A();
			break;
		}
		case 1: {
			x = new B();
			break;
		}
		case 2: {
			x = new C();
			break;
		}
		case 3: {
			x = new D();
			break;
		}
		default:
			x = new B();
		}
		return x.calculate(v1, v2, v3, v4);
	}
}

abstract class X {
	public int calculate(int v1, int v2, int v3, int v4) {
		long time = System.currentTimeMillis();
		int resp = this.process(v1, v2, v3, v4);
		time = System.currentTimeMillis() - time;
		log(resp, time);
		return resp;
	}

	private void log(int resp, long responseTime) {
		System.out.print(this.getClass().getName() + " : resp: " + resp + " responseTime:" + responseTime);
	}

	protected abstract int process(int v1, int v2, int v3, int v4);
}

class A extends X {
	public int process(int v1, int v2, int v3, int v4) {
		int amount = 0;
		for (int i = 0; i < v1; i++)
			for (int j = 0; j < v2; j++)
				for (int k = 0; k < v3; k++)
					for (int l = 0; l < v4; l++)
						amount++;

		return amount;

	}
}

class B extends X {
	public int process(int v1, int v2, int v3, int v4) {
		int a = 0, b = 0, c = 0, d = 0;
		for (int i = 0; i < v1; i++)
			a++;
		for (int i = 0; i < v2; i++)
			b++;
		for (int i = 0; i < v3; i++)
			c++;
		for (int i = 0; i < v4; i++)
			d++;
		return a * b * c * d;
	}
}

class C extends X {
	public int process(int v1, int v2, int v3, int v4) {
		int a = 0, b = 0;
		for (int i = 0; i < v1; i++)
			a++;

		for (int i = 0; i < v3; i++)
			b++;

		return a - v2 - b - v4;
	}
}

class D extends X {
	public int process(int v1, int v2, int v3, int v4) {
		return v1 / v2 / v3 / v4;
	}
}
